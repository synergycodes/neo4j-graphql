# Neo4J + GraphQL
Sample project with Neo4j (running by docker) + GraphQL

### Instalation
```
yarn
```

### Run database
```
docker-compose up
```

### Run application (GraphQL)
```
yarn start
```