const { Neo4jGraphQL } = require("@neo4j/graphql");
const { ApolloServer, gql } = require("apollo-server");
const neo4j = require("neo4j-driver");

const typeDefs = gql`
    type Team {
        name: String
        peoples: [Person] @relationship(type: "IN_TEAM", direction: IN)
    }

    type Position {
        name: String
        peoples: [Person] @relationship(type: "HAS_POSITION", direction: IN)
    }

    type Person {
        name: String
        surname: String
        positions: [Position] @relationship(type: "HAS_POSITION", direction: OUT)
        teams: [Team] @relationship(type: "IN_TEAM", direction: OUT)
    }
`;

const driver = neo4j.driver(
    "bolt://localhost:7687",
    neo4j.auth.basic("neo4j", "password")
);

const neoSchema = new Neo4jGraphQL({ typeDefs, driver });

const server = new ApolloServer({
    schema: neoSchema.schema,
});

server.listen().then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});